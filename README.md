# README #

Interview task with transfering money beetwen accounts.

### Technology stack ###

* Java8
* Dropwizard
* Guice (as DI)
* Flyway
* Maven
* H2
* Jpa(Hibernate)
* Lombok
* Junit/ Mockito/ AssertJ

### How do I get set up? ###

* mvn clean package
* java -jar transfer-web\target\transfer-web-1.0-SNAPSHOT.jar server config.yml

### Available endpoints ###

 * GET     /account/all
 * POST    /account/transfer
 * GET     /owner/all

### Description ###

During starting app flyway migrating ddl and dml scripts. After startup we have two owners:
* John Smith, id 1
* James Williams, id 2

Making money transfer:
[POST] http://localhost:8080/account/transfer
* Json Body
{
	"ownerId": 1,
	"receiverId": 2,
	"amount" : 12.34
}
