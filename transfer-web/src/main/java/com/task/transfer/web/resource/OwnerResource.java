package com.task.transfer.web.resource;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.task.transfer.core.service.OwnerService;


@Path("/owner")
@Produces(MediaType.APPLICATION_JSON)
public class OwnerResource {

    private final OwnerService ownerService;

    @Inject
    public OwnerResource(OwnerService ownerService) {
        this.ownerService = ownerService;
    }

    @GET
    @Path("/all")
    public Response findAllOwners() {
        return Response.ok(ownerService.findAll()).build();
    }
}
