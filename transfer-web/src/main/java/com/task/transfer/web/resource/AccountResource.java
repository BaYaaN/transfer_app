package com.task.transfer.web.resource;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.task.transfer.core.model.TransferModel;
import com.task.transfer.core.service.AccountService;


@Path("/account")
@Produces(MediaType.APPLICATION_JSON)
public class AccountResource {

    private final AccountService accountService;

    @Inject
    public AccountResource(final AccountService accountService) {
        this.accountService = accountService;
    }

    @GET
    @Path("/all")
    public Response findAllAccounts() {
        return Response.ok(accountService.findAll()).build();
    }

    @POST
    @Path("/transfer")
    @Consumes(MediaType.APPLICATION_JSON)
    public void transferMoney(@Valid final TransferModel transfer) {
        accountService.transfer(transfer);
    }
}
