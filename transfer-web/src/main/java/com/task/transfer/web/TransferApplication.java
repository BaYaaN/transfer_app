package com.task.transfer.web;


import com.google.inject.persist.PersistService;
import com.google.inject.persist.jpa.JpaPersistModule;
import com.hubspot.dropwizard.guice.GuiceBundle;
import com.task.transfer.core.config.TransferConfiguration;
import com.task.transfer.core.entity.AccountEntity;
import com.task.transfer.core.entity.OwnerEntity;
import com.task.transfer.web.resource.AccountResource;
import com.task.transfer.web.resource.OwnerResource;
import io.dropwizard.Application;
import io.dropwizard.configuration.ResourceConfigurationSourceProvider;
import io.dropwizard.db.DataSourceFactory;
import io.dropwizard.hibernate.HibernateBundle;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import org.flywaydb.core.Flyway;

public class TransferApplication extends Application<TransferConfiguration> {

    protected GuiceBundle<TransferConfiguration> guiceBundle;
    protected HibernateBundle<TransferConfiguration> hibernateBundle;
    private final String JPA_UNIT = "transfer";

    public static void main(String[] args) throws Exception {
        new TransferApplication().run(args);
    }

    @Override
    public String getName() {
        return "Transfer Application";
    }

    @Override
    public void initialize(Bootstrap<TransferConfiguration> bootstrap) {
        hibernateBundle = new HibernateBundle<TransferConfiguration>(OwnerEntity.class, AccountEntity.class) {
            public DataSourceFactory getDataSourceFactory(TransferConfiguration configuration) {
                return configuration.getDataSourceFactory();
            }
        };

        guiceBundle = GuiceBundle.<TransferConfiguration>newBuilder()
                .addModule(new JpaPersistModule(JPA_UNIT))
                .setConfigClass(TransferConfiguration.class)
                .build();

        bootstrap.setConfigurationSourceProvider(new ResourceConfigurationSourceProvider());
        bootstrap.addBundle(guiceBundle);
        bootstrap.addBundle(hibernateBundle);
    }

    @Override
    public void run(TransferConfiguration configuration, Environment environment) throws Exception {
        final DataSourceFactory dataSourceFactory = configuration.getDataSourceFactory();
        final Flyway flyway = new Flyway();
        flyway.setDataSource(dataSourceFactory.getUrl(),
                dataSourceFactory.getUser(),
                dataSourceFactory.getPassword());
        flyway.clean();
        flyway.migrate();
        environment.jersey().register(guiceBundle.getInjector().getInstance(OwnerResource.class));
        environment.jersey().register(guiceBundle.getInjector().getInstance(AccountResource.class));
        guiceBundle.getInjector().getInstance(PersistService.class).start();
    }
}
