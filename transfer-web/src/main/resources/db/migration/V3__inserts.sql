-- owners
INSERT INTO owner VALUES (1, 'John', 'Smith');
INSERT INTO owner VALUES (2, 'James', 'Williams');

-- accounts
INSERT INTO account VALUES (1, 1, 100.00);
INSERT INTO account VALUES (2, 2, 200.00);