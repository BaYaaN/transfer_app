CREATE TABLE account(
  id INTEGER NOT NULL,
  owner_id VARCHAR(25) NOT NULL,
  balance NUMERIC NOT NULL,
  PRIMARY KEY (id),
  FOREIGN KEY (owner_id) REFERENCES owner(id)
)