package com.task.transfer.web.config;


import com.google.inject.persist.jpa.JpaPersistModule;
import com.hubspot.dropwizard.guice.GuiceBundle;
import com.task.transfer.core.config.TransferConfiguration;
import com.task.transfer.core.entity.AccountEntity;
import com.task.transfer.core.entity.OwnerEntity;
import com.task.transfer.web.TransferApplication;
import io.dropwizard.db.DataSourceFactory;
import io.dropwizard.hibernate.HibernateBundle;
import io.dropwizard.setup.Bootstrap;

public class TestTransferApplication extends TransferApplication {

    private final String TEST_JPA_UNIT = "transfer";

    public static void main(String[] args) throws Exception {
        new TestTransferApplication().run(args);
    }


    @Override
    public void initialize(Bootstrap<TransferConfiguration> bootstrap) {
        hibernateBundle = new HibernateBundle<TransferConfiguration>(OwnerEntity.class, AccountEntity.class) {
            public DataSourceFactory getDataSourceFactory(TransferConfiguration configuration) {
                return configuration.getDataSourceFactory();
            }
        };

        guiceBundle = GuiceBundle.<TransferConfiguration>newBuilder()
                .addModule(new JpaPersistModule(TEST_JPA_UNIT))
                .setConfigClass(TransferConfiguration.class)
                .build();

        bootstrap.addBundle(guiceBundle);
        bootstrap.addBundle(hibernateBundle);
    }
}
