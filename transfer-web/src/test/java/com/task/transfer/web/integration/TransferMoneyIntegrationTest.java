package com.task.transfer.web.integration;


import javax.ws.rs.client.Client;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.Response;
import java.math.BigDecimal;
import java.util.List;

import com.task.transfer.core.config.TransferConfiguration;
import com.task.transfer.core.entity.AccountEntity;
import com.task.transfer.core.model.TransferModel;
import com.task.transfer.web.TransferApplication;
import com.task.transfer.web.config.TestTransferApplication;
import io.dropwizard.client.JerseyClientBuilder;
import io.dropwizard.testing.junit.DropwizardAppRule;
import org.apache.http.HttpStatus;
import org.junit.ClassRule;
import org.junit.Test;

import static io.dropwizard.testing.ResourceHelpers.resourceFilePath;
import static javax.ws.rs.client.Entity.json;
import static org.assertj.core.api.Assertions.assertThat;

public class TransferMoneyIntegrationTest {

    @ClassRule
    public static final DropwizardAppRule<TransferConfiguration> RULE =
            new DropwizardAppRule(TestTransferApplication.class, resourceFilePath("config.yml"));

    private final BigDecimal AMOUNT_TO_TRANSFER = new BigDecimal("20");
    private final BigDecimal OWNER_BALANCE_AFTER_TRANSFER = new BigDecimal("80.00");
    private final BigDecimal RECEIVER_BALANCE_AFTER_TRANSFER = new BigDecimal("220.00");
    private final Long OWNER_ID = 1L;
    private final Long RECEIVER_ID = 2L;
    private final Long NOT_EXISTING_ID = 28L;
    private final String HOST = "http://localhost:%d";
    private final String TRANSFER_PATH = HOST + "/account/transfer";
    private final String ACCOUNT_ALL_PATH = HOST + "/account/all";
    private final BigDecimal NEGATIVE_AMOUNT = new BigDecimal("-12.35");

    @Test
    public void shouldTransferMoney() {
        final Client client = new JerseyClientBuilder(RULE.getEnvironment()).build("transfer_client");

        //when
        final Response transferResponse = client.target(
                String.format(TRANSFER_PATH, RULE.getLocalPort()))
                .request()
                .post(json(TransferModel.builder()
                        .ownerId(OWNER_ID)
                        .receiverId(RECEIVER_ID)
                        .amount(AMOUNT_TO_TRANSFER)
                        .build()));

        final Response accountsResponse = client.target(
                String.format(ACCOUNT_ALL_PATH, RULE.getLocalPort()))
                .request()
                .get();


        //then
        assertThat(transferResponse.getStatus()).isEqualTo(HttpStatus.SC_NO_CONTENT);
        assertThat(accountsResponse.getStatus()).isEqualTo(HttpStatus.SC_OK);

        final List<AccountEntity> accounts = accountsResponse.readEntity(new GenericType<List<AccountEntity>>() {});
        assertThat(findAccountById(accounts, OWNER_ID).getBalance()).isEqualTo(OWNER_BALANCE_AFTER_TRANSFER);
        assertThat(findAccountById(accounts, RECEIVER_ID).getBalance()).isEqualTo(RECEIVER_BALANCE_AFTER_TRANSFER);
    }

    @Test
    public void shouldReturn500WhenOwnerDoesNotExist() {
        final Client client = new JerseyClientBuilder(RULE.getEnvironment()).build("wrong_owner_client");

        //when
        final Response transferResponse = client.target(
                String.format(TRANSFER_PATH, RULE.getLocalPort()))
                .request()
                .post(json(TransferModel.builder()
                        .ownerId(OWNER_ID)
                        .receiverId(NOT_EXISTING_ID)
                        .amount(AMOUNT_TO_TRANSFER)
                        .build()));

        //then
        assertThat(transferResponse.getStatus()).isEqualTo(HttpStatus.SC_INTERNAL_SERVER_ERROR);
    }

    @Test
    public void shouldReturn500WhenReceiverDoesNotExist() {
        final Client client = new JerseyClientBuilder(RULE.getEnvironment()).build("wrong_receiver_client");

        //when
        final Response transferResponse = client.target(
                String.format(TRANSFER_PATH, RULE.getLocalPort()))
                .request()
                .post(json(TransferModel.builder()
                        .ownerId(NOT_EXISTING_ID)
                        .receiverId(RECEIVER_ID)
                        .amount(AMOUNT_TO_TRANSFER)
                        .build()));

        //then
        assertThat(transferResponse.getStatus()).isEqualTo(HttpStatus.SC_INTERNAL_SERVER_ERROR);
    }

    @Test
    public void shouldNotValidateWhenOwnerIdIsNull() {
        //given
        final Client client = new JerseyClientBuilder(RULE.getEnvironment()).build("owner id validation");
        final TransferModel transferModel = TransferModel.builder()
                .ownerId(null)
                .receiverId(RECEIVER_ID)
                .amount(AMOUNT_TO_TRANSFER)
                .build();

        //when
        final Response post = client.target(
                String.format(TRANSFER_PATH, RULE.getLocalPort()))
                .request()
                .post(json(transferModel));

        //then
        assertThat(post.getStatus()).isEqualTo(HttpStatus.SC_UNPROCESSABLE_ENTITY);
    }

    @Test
    public void shouldNotValidateWhenReceiverIdIsNull() {
        //given
        final Client client = new JerseyClientBuilder(RULE.getEnvironment()).build("receiver id validation");
        final TransferModel transferModel = TransferModel.builder()
                .ownerId(NOT_EXISTING_ID)
                .receiverId(null)
                .amount(AMOUNT_TO_TRANSFER)
                .build();

        //when
        final Response post = client.target(
                String.format(TRANSFER_PATH, RULE.getLocalPort()))
                .request()
                .post(json(transferModel));

        //then
        assertThat(post.getStatus()).isEqualTo(HttpStatus.SC_UNPROCESSABLE_ENTITY);
    }

    @Test
    public void shouldNotValidateWhenAmountIsNull() {
        //given
        final Client client = new JerseyClientBuilder(RULE.getEnvironment()).build("amount id validation");
        final TransferModel transferModel = TransferModel.builder()
                .ownerId(NOT_EXISTING_ID)
                .receiverId(RECEIVER_ID)
                .amount(null)
                .build();


        //when
        final Response post = client.target(
                String.format(TRANSFER_PATH, RULE.getLocalPort()))
                .request()
                .post(json(transferModel));

        //then
        assertThat(post.getStatus()).isEqualTo(HttpStatus.SC_UNPROCESSABLE_ENTITY);
    }

    @Test
    public void shouldNotValidateWhenAmountIsPositive() {
        //given
        final Client client = new JerseyClientBuilder(RULE.getEnvironment()).build("negative amount validation");
        final TransferModel transferModel = TransferModel.builder()
                .ownerId(NOT_EXISTING_ID)
                .receiverId(RECEIVER_ID)
                .amount(NEGATIVE_AMOUNT)
                .build();

        //when
        final Response post = client.target(
                String.format(TRANSFER_PATH, RULE.getLocalPort()))
                .request()
                .post(json(transferModel));

        //then
        assertThat(post.getStatus()).isEqualTo(HttpStatus.SC_UNPROCESSABLE_ENTITY);
    }

    private AccountEntity findAccountById(List<AccountEntity> accounts, Long id) {
        return accounts
                .stream()
                .filter(account -> account.getId().equals(id))
                .findFirst()
                .get();
    }

}
