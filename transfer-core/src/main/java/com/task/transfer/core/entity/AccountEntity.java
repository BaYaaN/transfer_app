package com.task.transfer.core.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;

import lombok.Data;

@Data
@Entity
@Table(name = "account")
@NamedQueries({
      @NamedQuery(name = "Account.findByOwner", query = "SELECT account FROM AccountEntity account WHERE account.owner = :owner"),
      @NamedQuery(name = "Account.findAll", query = "SELECT account FROM AccountEntity account")
})
public class AccountEntity implements Serializable {
    @Id
    private Long id;

    @NotNull
    @OneToOne
    @JoinColumn(name = "owner_id")
    private OwnerEntity owner;

    @NotNull
    @Column(name = "balance")
    private BigDecimal balance;
}
