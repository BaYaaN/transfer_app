package com.task.transfer.core.service;


import javax.inject.Inject;
import java.util.List;
import java.util.NoSuchElementException;

import com.google.inject.Singleton;

import com.task.transfer.core.entity.OwnerEntity;
import com.task.transfer.core.repository.OwnerRepository;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Singleton
public class OwnerService {

    private final OwnerRepository ownerRepository;

    @Inject
    public OwnerService(final OwnerRepository ownerRepository) {
        this.ownerRepository = ownerRepository;
    }

    public OwnerEntity findOne(@NonNull final Long id) {
        log.info("Retrieve owner with id: {} from db", id);
        return ownerRepository
                .findOne(id)
                .orElseThrow(() -> new NoSuchElementException("Owner with id " + id + "does not exist"));
    }

    public List<OwnerEntity> findAll() {
        log.info("Retrieving all owners from db");
        return ownerRepository.findAll();
    }
}
