package com.task.transfer.core.repository;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.util.List;
import java.util.Optional;

import com.google.inject.Provider;
import com.google.inject.Singleton;
import com.google.inject.persist.Transactional;

import com.task.transfer.core.entity.OwnerEntity;
import lombok.NonNull;

@Singleton
@Transactional
public class OwnerRepository {

    private final Provider<EntityManager> entityManagerProvider;

    @Inject
    public OwnerRepository(final Provider<EntityManager> entityManagerProvider) {
        this.entityManagerProvider = entityManagerProvider;
    }

    public Optional<OwnerEntity> findOne(@NonNull final Long id) {
        return Optional.ofNullable(entityManagerProvider.get().find(OwnerEntity.class, id));
    }

    public List<OwnerEntity> findAll() {
        return entityManagerProvider.get().createNamedQuery("Owner.findAll", OwnerEntity.class).getResultList();
    }
}
