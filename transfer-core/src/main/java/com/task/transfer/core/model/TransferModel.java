package com.task.transfer.core.model;


import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class TransferModel {
    @NotNull
    private Long ownerId;

    @NotNull
    private Long receiverId;

    @NotNull
    @Min(value = 0L, message = "The amount must be positive")
    private BigDecimal amount;
}
