package com.task.transfer.core.service;


import javax.inject.Inject;
import java.math.BigDecimal;
import java.util.List;
import java.util.NoSuchElementException;

import com.google.inject.Singleton;
import com.google.inject.persist.Transactional;
import com.task.transfer.core.entity.AccountEntity;
import com.task.transfer.core.model.TransferModel;
import com.task.transfer.core.repository.AccountRepository;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Singleton
public class AccountService {

    private final AccountRepository accountRepository;
    private final OwnerService ownerService;

    @Inject
    public AccountService(final AccountRepository accountRepository, OwnerService ownerService) {
        this.accountRepository = accountRepository;
        this.ownerService = ownerService;
    }

    @Transactional(rollbackOn = RuntimeException.class)
    public void transfer(@NonNull final TransferModel transferModel) {
        log.info("Start processing money transfer from user: {} to receiver: {}. Transfer amount: {}", transferModel.getOwnerId(), transferModel
                .getReceiverId(), transferModel.getAmount());

        final AccountEntity ownerAccount = findByOwner(transferModel.getOwnerId());
        final AccountEntity receiverAccount = findByOwner(transferModel.getReceiverId());

        log.info("Owner balancer before transfer: {}. Receiver balance before transfer {}", ownerAccount.getBalance(), receiverAccount.getBalance());

        if (ownerAccount.getBalance().compareTo(transferModel.getAmount()) < 0) {
            throw new IllegalArgumentException("Owner has insufficient amount of money");
        }

        synchronized (ownerAccount) {
            withdrawFromOwnerAccount(ownerAccount, transferModel.getAmount());
        }
        synchronized (receiverAccount) {
            depositToReceiverAccount(receiverAccount, transferModel.getAmount());
        }

        log.info("Transfer money finished successful");
        log.info("Owner balancer after transfer: {}. Receiver balance after transfer {}", ownerAccount.getBalance(), receiverAccount.getBalance());
    }

    public List<AccountEntity> findAll() {
        log.info("Retrieving all accounts from db");
        return accountRepository.findAll();
    }

    private AccountEntity findByOwner(final Long ownerId) {
        return accountRepository
                .findByOwner(ownerService.findOne(ownerId))
                .orElseThrow(() -> new NoSuchElementException("Owner with id " + ownerId + "does not have account"));
    }

    private void withdrawFromOwnerAccount(final AccountEntity owner, final BigDecimal transferAmount) {
        owner.setBalance(owner.getBalance().subtract(transferAmount));
        accountRepository.update(owner);
    }

    private void depositToReceiverAccount(final AccountEntity receiver, final BigDecimal transferAmount) {
        receiver.setBalance(receiver.getBalance().add(transferAmount));
        accountRepository.update(receiver);
    }
}
