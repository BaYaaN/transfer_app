package com.task.transfer.core.repository;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.util.List;
import java.util.Optional;

import com.google.inject.Provider;
import com.google.inject.Singleton;
import com.google.inject.persist.Transactional;
import com.task.transfer.core.entity.AccountEntity;
import com.task.transfer.core.entity.OwnerEntity;
import lombok.NonNull;

@Singleton
@Transactional
public class AccountRepository {
    private final Provider<EntityManager> entityManagerProvider;

    @Inject
    public AccountRepository(final Provider<EntityManager> entityManagerProvider) {
        this.entityManagerProvider = entityManagerProvider;
    }

    public void update(@NonNull final AccountEntity account) {
        entityManagerProvider.get().merge(account);
    }

    public Optional<AccountEntity> findByOwner(@NonNull final OwnerEntity owner) {
        return Optional.ofNullable(entityManagerProvider.get().createNamedQuery("Account.findByOwner", AccountEntity.class).setParameter("owner", owner)
                .getSingleResult());
    }

    public List<AccountEntity> findAll() {
        return entityManagerProvider.get().createNamedQuery("Account.findAll").getResultList();
    }
}
