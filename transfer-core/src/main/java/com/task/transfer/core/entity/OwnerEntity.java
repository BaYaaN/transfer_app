package com.task.transfer.core.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

import lombok.Data;

@Data
@Entity
@Table(name = "owner")
@NamedQuery(name = "Owner.findAll", query = "SELECT owner FROM OwnerEntity owner")
public class OwnerEntity implements Serializable {

    @Id
    private Long id;

    @NotNull
    @Size(max = 50)
    @Column(name = "first_name")
    private String firstName;

    @NotNull
    @Size(max = 50)
    @Column(name = "last_name")
    private String lastName;
}
