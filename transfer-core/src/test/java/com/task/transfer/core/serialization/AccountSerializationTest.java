package com.task.transfer.core.serialization;


import java.io.IOException;
import java.math.BigDecimal;

import com.fasterxml.jackson.databind.ObjectMapper;

import com.task.transfer.core.common.TestDataBuilder;
import com.task.transfer.core.entity.AccountEntity;
import io.dropwizard.jackson.Jackson;
import org.hamcrest.core.Is;
import org.junit.Test;


import static io.dropwizard.testing.FixtureHelpers.fixture;
import static org.hamcrest.MatcherAssert.assertThat;
import static uk.co.datumedge.hamcrest.json.SameJSONAs.sameJSONAs;

public class AccountSerializationTest {

    protected final ObjectMapper MAPPER = Jackson.newObjectMapper();

    @Test
    public void serializesToJSON() throws Exception {
        //given
        final AccountEntity accountEntity = TestDataBuilder.account(1L, TestDataBuilder.owner(1L, "Bruce", "Willis"), new BigDecimal("100.0"));

        //then
        assertThat(MAPPER.writeValueAsString(accountEntity), sameJSONAs(fixture("fixtures/account.json")));
    }

    @Test
    public void deserializesFromJSON() throws IOException {
        assertThat(MAPPER.readValue(fixture("fixtures/account.json"), AccountEntity.class), Is.isA(AccountEntity.class));
    }
}
