package com.task.transfer.core.common;


import java.math.BigDecimal;


import com.task.transfer.core.entity.AccountEntity;
import com.task.transfer.core.entity.OwnerEntity;
import com.task.transfer.core.model.TransferModel;
import lombok.NonNull;

public class TestDataBuilder {

    public static TransferModel transferModel(@NonNull final Long ownerId, @NonNull final Long receiverId, @NonNull final BigDecimal amount) {
        return TransferModel
                .builder()
                .ownerId(ownerId)
                .receiverId(receiverId)
                .amount(amount)
                .build();
    }

    public static OwnerEntity owner(@NonNull final Long id, @NonNull final String firstName, @NonNull final String lastName) {
        OwnerEntity owner = new OwnerEntity();
        owner.setId(id);
        owner.setFirstName(firstName);
        owner.setLastName(lastName);

        return owner;
    }

    public static AccountEntity account(@NonNull final Long id, @NonNull final OwnerEntity owner, @NonNull final BigDecimal balance) {
        AccountEntity account = new AccountEntity();
        account.setId(id);
        account.setOwner(owner);
        account.setBalance(balance);

        return account;
    }
}
