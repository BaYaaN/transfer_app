package com.task.transfer.core.service.account;

import java.math.BigDecimal;
import java.util.List;

import com.task.transfer.core.entity.AccountEntity;
import com.task.transfer.core.entity.OwnerEntity;
import com.task.transfer.core.repository.AccountRepository;
import com.task.transfer.core.service.AccountService;
import org.assertj.core.util.Lists;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static com.task.transfer.core.common.TestDataBuilder.account;
import static com.task.transfer.core.common.TestDataBuilder.owner;
import static org.assertj.core.api.Java6Assertions.assertThat;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class FindAllTest {

    @InjectMocks
    private AccountService accountService;

    @Mock
    private AccountRepository accountRepository;


    @Test
    public void shouldReturnAllAccounts() {
        //given
        final OwnerEntity ownerEntity = owner(1L, "Bruce", "Willis");
        final List<AccountEntity> accounts = Lists.newArrayList(account(1L, ownerEntity, new BigDecimal("20")),
                account(1L, ownerEntity, new BigDecimal("20")));
        when(accountRepository.findAll()).thenReturn(accounts);

        //when
        final List<AccountEntity> result = accountService.findAll();

        //then
        assertThat(result).hasSameSizeAs(accounts);
        assertThat(result).hasSameElementsAs(accounts);
    }

    @Test
    public void shouldReturnEmptyList() {
        //given
        final List<AccountEntity> accounts = Lists.emptyList();
        when(accountRepository.findAll()).thenReturn(accounts);

        //when
        final List<AccountEntity> result = accountService.findAll();

        //then
        assertThat(result).isEmpty();
    }
}
