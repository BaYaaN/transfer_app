package com.task.transfer.core.service.account;


import java.math.BigDecimal;
import java.util.Optional;


import com.task.transfer.core.common.TestDataBuilder;
import com.task.transfer.core.entity.AccountEntity;
import com.task.transfer.core.repository.AccountRepository;
import com.task.transfer.core.service.AccountService;
import com.task.transfer.core.entity.OwnerEntity;
import com.task.transfer.core.model.TransferModel;
import com.task.transfer.core.service.OwnerService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static com.task.transfer.core.common.TestDataBuilder.account;
import static com.task.transfer.core.common.TestDataBuilder.owner;
import static com.task.transfer.core.common.TestDataBuilder.transferModel;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TransferTest {

    @InjectMocks
    private AccountService accountService;

    @Mock
    private OwnerService ownerService;

    @Mock
    private AccountRepository accountRepository;


    @Test
    public void shouldThrownExceptionWhenOwnerHasInsufficientBalance() {
        //given
        OwnerEntity ownerEntity = owner(1L, "Bruce", "Willis");
        OwnerEntity receiver = owner(2L, "Chuck", "Norris");
        AccountEntity ownerAccount = account(1L, ownerEntity, new BigDecimal("20"));
        TransferModel transfer = transferModel(1L, 2L, new BigDecimal("100.00"));
        when(ownerService.findOne(1L)).thenReturn(ownerEntity);
        when(ownerService.findOne(2L)).thenReturn(receiver);
        when(accountRepository.findByOwner(any())).thenReturn(Optional.of(ownerAccount));

        //then
        assertThatThrownBy(() -> accountService.transfer(transfer))
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessageContaining("Owner has insufficient amount of money");
    }
}
