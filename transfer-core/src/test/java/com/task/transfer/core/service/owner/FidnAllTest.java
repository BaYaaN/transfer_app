package com.task.transfer.core.service.owner;

import java.util.List;

import com.task.transfer.core.entity.OwnerEntity;
import com.task.transfer.core.repository.OwnerRepository;
import com.task.transfer.core.service.OwnerService;
import org.assertj.core.util.Lists;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static com.task.transfer.core.common.TestDataBuilder.owner;
import static org.assertj.core.api.Java6Assertions.assertThat;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class FidnAllTest {
    @InjectMocks
    private OwnerService ownerService;

    @Mock
    private OwnerRepository ownerRepository;


    @Test
    public void shouldReturnAllOwners() {
        //given
        final List<OwnerEntity> owners = Lists.newArrayList(owner(1L, "Bruce", "Willis"),
                owner(2L, "Chuck", "Norris"));
        when(ownerRepository.findAll()).thenReturn(owners);

        //when
        final List<OwnerEntity> result = ownerService.findAll();

        //then
        assertThat(result).hasSameSizeAs(owners);
        assertThat(result).hasSameElementsAs(owners);
    }

    @Test
    public void shouldReturnEmptyList() {
        //given
        final List<OwnerEntity> owners = Lists.emptyList();
        when(ownerRepository.findAll()).thenReturn(owners);

        //when
        final List<OwnerEntity> result = ownerService.findAll();

        //then
        assertThat(result).isEmpty();
    }
}
