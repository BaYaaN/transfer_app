package com.task.transfer.core.service.owner;


import java.util.Optional;

import com.task.transfer.core.entity.OwnerEntity;
import com.task.transfer.core.repository.OwnerRepository;
import com.task.transfer.core.service.OwnerService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static com.task.transfer.core.common.TestDataBuilder.owner;
import static org.assertj.core.api.Java6Assertions.assertThat;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class FindOneTest {
    @InjectMocks
    private OwnerService ownerService;

    @Mock
    private OwnerRepository ownerRepository;


    @Test
    public void shouldReturnOwner() {
        //given
        final OwnerEntity owner = owner(1L, "Bruce", "Willis");
        when(ownerRepository.findOne(1L)).thenReturn(Optional.of(owner));

        //when
        final OwnerEntity result = ownerService.findOne(1L);

        //then
        assertThat(result).isSameAs(owner);
    }
}
