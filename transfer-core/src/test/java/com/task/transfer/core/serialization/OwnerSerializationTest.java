package com.task.transfer.core.serialization;

import java.io.IOException;

import com.fasterxml.jackson.databind.ObjectMapper;

import com.task.transfer.core.common.TestDataBuilder;
import com.task.transfer.core.entity.OwnerEntity;
import io.dropwizard.jackson.Jackson;
import org.hamcrest.core.Is;
import org.junit.Test;


import static io.dropwizard.testing.FixtureHelpers.fixture;
import static org.hamcrest.MatcherAssert.assertThat;
import static uk.co.datumedge.hamcrest.json.SameJSONAs.sameJSONAs;


public class OwnerSerializationTest {

    private final ObjectMapper MAPPER = Jackson.newObjectMapper();

    @Test
    public void serializesToJSON() throws Exception {
        //given
        final OwnerEntity ownerEntity = TestDataBuilder.owner(1L, "Bruce", "Willis");

        //then
        assertThat(MAPPER.writeValueAsString(ownerEntity), sameJSONAs(fixture("fixtures/owner.json")));
    }

    @Test
    public void deserializedFromJSON() throws IOException {
        assertThat(MAPPER.readValue(fixture("fixtures/owner.json"), OwnerEntity.class), Is.isA(OwnerEntity.class));
    }
}
